#include "pch.h"
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:4996)
#include<math.h>
#include<conio.h>
#include <graphics.h> 
#define PI 3.1415926
#define width 640
#define high  720
int center_x = width / 2, center_y = high / 2, radius = 300;
int SecondLength = 280, MinuteLength = 200, HourLength = 100;
double SecondAngle, MinuteAngle, HourAngle;
void show1(double SecondAngle, double MinuteAngle, double HourAngle)
{
	setlinecolor(RED);
	setlinestyle(PS_SOLID, 3);
	line(center_x, center_y, center_x + sin(SecondAngle)*SecondLength, center_y - SecondLength * cos(SecondAngle));
	setlinecolor(LIGHTBLUE);
	setlinestyle(PS_SOLID, 5);
	line(center_x, center_y, center_x + sin(MinuteAngle)*MinuteLength, center_y - MinuteLength * cos(MinuteAngle));
	setlinecolor(WHITE);
	setlinestyle(PS_SOLID, 10);
	line(center_x, center_y, center_x + sin(HourAngle)*HourLength, center_y - HourLength * cos(HourAngle));
}
void main()
{
	initgraph(width, high);
	setcolor(BLUE);
	setfillcolor(LIGHTBLUE);
	fillcircle(center_x, center_y, 300);
	settextstyle(40, 0, 0);
	settextcolor(YELLOW);
	outtextxy(center_x - 140, center_y + 120, _T("这是钟表吗？"));
	outtextxy(310, 100, _T("XII"));
	outtextxy(300, 590, _T("VI"));
	outtextxy(550, 340, _T("III"));
	outtextxy(50, 340, _T("IX"));
	outtextxy(90, 220, _T("X"));
	outtextxy(170, 140, _T("XI"));
	outtextxy(460, 140, _T("I"));
	outtextxy(520, 220, _T("II"));
	outtextxy(520, 470, _T("IV"));
	outtextxy(430, 540, _T("V"));
	outtextxy(180, 550, _T("VII"));
	outtextxy(100, 460, _T("VIII"));
	setwritemode(R2_XORPEN);
	SYSTEMTIME ti;
	BeginBatchDraw();
	while (!kbhit())
	{
		GetLocalTime(&ti);
		SecondAngle = ti.wSecond * PI * 2 / 60;		//秒针
		MinuteAngle = ti.wMinute * PI * 2 / 60;		//分针
		HourAngle = ti.wHour * PI * 2 / 12;			//时针
		show1(SecondAngle, MinuteAngle, HourAngle);
		FlushBatchDraw(); Sleep(100);
		show1(SecondAngle, MinuteAngle, HourAngle);
	}
	EndBatchDraw();
	closegraph();
}